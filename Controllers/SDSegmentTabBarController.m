//
//  SDTabBarController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "SDSegmentTabBarController.h"

@interface SDSegmentTabBarController ()
{
    UIView * tabOverlay;
    UISegmentedControl * tabSegment;
}
@end

@implementation SDSegmentTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setViewControllers:(NSArray *)viewControllers
{
    [super setViewControllers:viewControllers];
    CGRect rect = self.tabBar.frame;
    rect.origin.y = 0;
    tabOverlay = [[UIView alloc] initWithFrame:rect];
    [tabOverlay setBackgroundColor:[UIColor colorWithRed:112/255.0f green:0/255.0f blue:7/255.0f alpha:1.0f]];
    
    NSMutableArray * segments = [[NSMutableArray alloc] init];
    for (UIViewController * controller in viewControllers) {
        [segments addObject:controller.title];
    }
    
    tabSegment = [[UISegmentedControl alloc] initWithItems:segments];
    [tabSegment setFrame:CGRectMake(10, 10, rect.size.width - 20, rect.size.height - 20)];
    
    [tabSegment setTintColor:[UIColor whiteColor]];
    [tabSegment setSelectedSegmentIndex:0];
    [tabSegment addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    [tabOverlay addSubview:tabSegment];
    [self.tabBar addSubview:tabOverlay];
    
}

-(void) segmentValueChanged:(id)sender
{
    NSInteger selectedIndex = tabSegment.selectedSegmentIndex;
    [self setSelectedIndex:selectedIndex];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
