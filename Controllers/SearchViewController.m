//
//  SearchViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "SearchViewController.h"
#import "ResturantTableViewCell.h"
#import "DMResturant.h"
#import "DMDeal.h"
#import "UIImageView+AFNetworking.h"
#import "ResturantDetailViewController.h"
#import "SDAnnotation.h"
#import "DealsDetailViewController.h"
#import "DMCategory.h"
#import "ActionSheetStringPicker.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"


@interface SearchViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, MKMapViewDelegate, UITextFieldDelegate>
{
    NSMutableArray * dataArray;
    NSMutableArray * searchArray;
    NSMutableArray * categoriesArray;
    AppDelegate * app;
    PFGeoPoint * userLocation;
}
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self .title = NSLocalizedString(@"search", @"Search");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dataArray = [[NSMutableArray alloc] init];
    searchArray = [[NSMutableArray alloc] init];
    categoriesArray = [[NSMutableArray alloc] init];
    app = [UIApplication sharedApplication].delegate;
    [searchArray addObject:_cellMap];
    
    if (app.currentLocation == nil) {
        [self getUserLocation];
    }
    else{
        userLocation = app.currentLocation;
    }
    
    [_txtCity setPlaceholder:NSLocalizedString(@"city", @"City")];
    [_txtKitchen setPlaceholder:NSLocalizedString(@"kitchen", @"Kitchen")];
    [_txtName setPlaceholder:NSLocalizedString(@"name", @"Name")];
    [_btnSearch setTitle:NSLocalizedString(@"search", @"Search") forState:UIControlStateNormal];
    [self getNearestResturants];
    
    if (_searchType == SearchTypeSok) {
        [_txtCity setHidden:YES];
        [_txtName setHidden:YES];
        _txtKitchen.translatesAutoresizingMaskIntoConstraints = YES;
        
        CGRect rect = _txtKitchen.frame;
        rect.origin.x = 10;
        rect.size.width = [UIScreen mainScreen].bounds.size.width - 20;
        [_txtKitchen setFrame:rect];
    }
}

-(void) getUserLocation
{
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if (!error) {
            userLocation = geoPoint;
            app.currentLocation = geoPoint;
        }
        else{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [Utility showAlert:NSLocalizedString(@"LocationError", @"Location Error") withTitle:NSLocalizedString(@"appName", @"Application Name")];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addAnnotations
{
    CLLocationCoordinate2D lastLocation;
    NSInteger tag = 0;
    for (DMResturant * resturant in dataArray) {
        lastLocation = CLLocationCoordinate2DMake(resturant.map.latitude, resturant.map.longitude);
        SDAnnotation * annotation = [[SDAnnotation alloc] initWithTitle:resturant.name AndCoordinate:lastLocation];
        annotation.extraData = resturant;
        annotation.tag = tag++;
        [self.mapView addAnnotation:annotation];
    }
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = lastLocation;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [_mapView setRegion:mapRegion animated: YES];
}

-(void)resignAllResponders
{
    [_txtCity resignFirstResponder];
    [_txtKitchen resignFirstResponder];
    [_txtName resignFirstResponder];
    
    
    [_txtKitchen endEditing:YES];
    [_txtCity endEditing:YES];
    [_txtName endEditing:YES];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:_txtKitchen]) {
        [self resignAllResponders];
        NSMutableArray * catArray = [[NSMutableArray alloc] init];
        for (DMCategory * cat in categoriesArray) {
            [catArray addObject:cat.name];
        }
        
        [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"selectCategory", @"Select Category") rows:catArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            if (selectedIndex == 0) {
                _txtKitchen.text = @"";
            }
            else
                _txtKitchen.text = selectedValue;
            
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
        
        return NO;
    }
    return YES;
}

#pragma mark - IBActions
- (IBAction)searchClicked:(id)sender {
    if ([_txtName.text isEqualToString:@""] && [_txtCity.text isEqualToString:@""] && [_txtKitchen.text isEqualToString:@""]) {
        [Utility showAlert:NSLocalizedString(@"searchRequired", @"Search Required") withTitle:NSLocalizedString(@"appName", @"Application Name")];
    }
    else
    {
        
        [self resignAllResponders];
        [self searchResturants:_txtName.text withCity:_txtCity.text];
    }
}

- (IBAction)exitOnEnd:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)edittingBegin:(id)sender {
    [self resignAllResponders];
    
    
}

-(NSString *) getRestCategoriesString:(DMResturant*)rest
{
    NSString * category = @"";
    for (int i=0;i< rest.categories.count;i++) {
        DMCategory * cat = [rest.categories objectAtIndex:i];
        if (i != 0) {
            category = [category stringByAppendingString:@"/"];
        }
        category = [category stringByAppendingString:cat.name];
    }
    return category;
}

-(NSString *) getDealCategoriesString:(DMDeal*)rest
{
    NSString * category = @"";
    for (int i=0;i< rest.categories.count;i++) {
        DMCategory * cat = [rest.categories objectAtIndex:i];
        if (i != 0) {
            category = [category stringByAppendingString:@"/"];
        }
        category = [category stringByAppendingString:cat.name];
    }
    return category;
}

#pragma mark - API Method
-(void) searchResturants:(NSString*)key withCity:(NSString*)city
{
    NSString * methodName = @"searchResturants";
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    if (![key isEqualToString:@""]) {
        key = [NSString stringWithFormat:@"(.)*%@(.)*", key];
        [params setObject:key forKey:@"query"];
    }
    if (![city isEqualToString:@""]){
        city = [NSString stringWithFormat:@"(.)*%@(.)*", city];
        [params setObject:city forKey:@"city"];
    }
    
    if (![_txtKitchen.text isEqualToString:@""]) {
        [params setObject:_txtKitchen.text forKey:@"category"];
        methodName = @"searchResturantsWithCategory";
    }
    
    
    [PFCloud callFunctionInBackground:methodName withParameters:params block:^(NSArray *result, NSError *error) {
        if (!error) {
            [searchArray removeAllObjects];
            [searchArray addObject:_cellMap];
            
            for (PFObject *object in result) {
                DMResturant * resturant = [[DMResturant alloc] initWithObject:object];
                [searchArray addObject:resturant];
            }
            [self searchDeals:key withCity:city];
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Internet Error") withTitle:NSLocalizedString(@"appName", @"App Name")];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)searchDeals:(NSString*)key withCity:(NSString*)city
{
    NSString * methodName = @"searchDeals";
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    if (![key isEqualToString:@""]) {
        key = [NSString stringWithFormat:@"(.)*%@(.)*", key];
        [params setObject:key forKey:@"query"];
    }
    if (![city isEqualToString:@""]){
        city = [NSString stringWithFormat:@"(.)*%@(.)*", city];
        [params setObject:city forKey:@"city"];
    }
    
    if (![_txtKitchen.text isEqualToString:@""]) {
        [params setObject:_txtKitchen.text forKey:@"category"];
        methodName = @"searchDealsWithCategory";
    }
    
    [PFCloud callFunctionInBackground:methodName withParameters:params block:^(NSArray *result, NSError *error) {
        if (!error) {
            
            for (PFObject *object in result) {
                DMDeal * resturant = [[DMDeal alloc] initWithObject:object];
                [searchArray addObject:resturant];
            }
            [self.mTableView reloadData];
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Internet Error")  withTitle:NSLocalizedString(@"appName", @"App Name")];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void) getNearestResturants
{
    PFQuery * query = [PFQuery queryWithClassName:@"Resturant"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                
                DMResturant * resturant = [[DMResturant alloc] initWithPFObject:object];
                [dataArray addObject:resturant];
                
            }
            [self addAnnotations];
            [self getCategories];
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Internet Error")  withTitle:NSLocalizedString(@"appName", @"App Name")];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void) getCategories
{
    
    PFQuery * query = [PFQuery queryWithClassName:@"Category"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            DMCategory * nonCategory = [[DMCategory alloc] init];
            nonCategory.name = @"None";
            [categoriesArray addObject:nonCategory];
            
            for (PFObject *object in objects) {
                
                DMCategory * category = [[DMCategory alloc] initWithObject:object];
                [categoriesArray addObject:category];
            }
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Internet Error")  withTitle:NSLocalizedString(@"appName", @"App Name")];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return _cellMap;
    }
    ResturantTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"resturantCell"];
    if (!cell) {
        NSArray * nibsArray = [[NSBundle mainBundle] loadNibNamed:@"ResturantTableViewCell" owner:nil options:nil];
        cell = [nibsArray objectAtIndex:0];
    }
    
    id object = [searchArray objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[DMDeal class]]) {
        DMDeal * deal = object;
        
        [cell.imgLogo setImageWithURL:[NSURL URLWithString:deal.logoImage.url] placeholderImage:[UIImage imageNamed:@"no_image"]];
        [cell.lblName setText:deal.deal];
        [cell.lblDescription setText:deal.description];
        [cell.lblCategory setText:[self getDealCategoriesString:deal]];
        
        float distance = [Utility distanceFromLocation:userLocation toLocation:deal.geoLocation];
        
        [cell.lblDistance setText:[NSString stringWithFormat:@"%.2f KM", distance]];
    }
    else if ([object isKindOfClass:[DMResturant class]]){
        DMResturant * resturant = object;
        [cell.imgLogo setImageWithURL:[NSURL URLWithString:resturant.logoImage.url] placeholderImage:[UIImage imageNamed:@"no_image"]];
        [cell.lblName setText:resturant.name];
        [cell.lblDescription setText:resturant.shortDescription];
        [cell.lblCategory setText:[self getRestCategoriesString:resturant]];
        float distance = [Utility distanceFromLocation:userLocation toLocation:resturant.map];
        
        [cell.lblDistance setText:[NSString stringWithFormat:@"%.2f KM", distance]];
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==0) {
        return 150;
    }
    return 80;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return;
    }
    
    id object = [searchArray objectAtIndex:indexPath.row];
    
    if ([object isKindOfClass:[DMDeal class]]) {
        DMDeal * deal= [searchArray objectAtIndex:indexPath.row];
        DealsDetailViewController * detailController = [[DealsDetailViewController alloc] init];
        detailController.currentDeal = deal;
        [self.navigationController pushViewController:detailController animated:YES];
    }
    else if ([object isKindOfClass:[DMResturant class]]){
        DMResturant * resturant= object;
        ResturantDetailViewController * detailController = [[ResturantDetailViewController alloc] init];
        detailController.currentResturant = resturant;
        [self.navigationController pushViewController:detailController animated:YES];
    }
    
    
}

#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self searchResturants:searchBar.text withCity:_txtCity.text];
}

#pragma mark - MapViewDelegate
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *annView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"pinview"];
    if (annView == nil)
    {
        annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pinview"];
        
        annView.animatesDrop = YES;
        annView.canShowCallout = YES;
        
        UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        SDAnnotation * ann = (SDAnnotation*)annotation;
        detailButton.tag = ann.tag;
        annView.rightCalloutAccessoryView=detailButton;
    }
    else {
        annView.annotation = annotation;
    }
    //annView.tag = ((SDAnnotation*)annotation).tag;
    
    return annView;
}
-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"Detail view clickec");
    DMResturant * resturant = [dataArray objectAtIndex:control.tag];
    
    ResturantDetailViewController * resturantController = [[ResturantDetailViewController alloc] init];
    resturantController.currentResturant = resturant;
    [self.navigationController pushViewController:resturantController animated:YES];
    
}

@end
