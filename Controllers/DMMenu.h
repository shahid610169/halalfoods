//
//  DMMenu.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 08/09/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMCategory.h"

@interface DMMenu : NSObject

@property (nonatomic, copy) NSString * objectId;
@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * description;
@property (nonatomic, strong) DMCategory * category;

-(id)initWithObject:(PFObject*)obj;

@end
