//
//  SearchViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseMenuViewController.h"
#import <MapKit/MapKit.h>

typedef enum {
    SearchTypeDefault,
    SearchTypeSok
}SearchType;

@interface SearchViewController : BaseMenuViewController

@property (nonatomic) SearchType searchType;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UITextField *txtKitchen;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMap;

- (IBAction)searchClicked:(id)sender;
- (IBAction)exitOnEnd:(id)sender;
- (IBAction)edittingBegin:(id)sender;

@end
