//
//  ResturantDetailViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMResturant.h"
#import "BaseViewController.h"
#import <MapKit/MapKit.h>

@interface ResturantDetailViewController : BaseViewController
// Cell References
@property (strong, nonatomic) IBOutlet UITableViewCell *cellImage;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAddress;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellOpeningHours;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellPhoneNo;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellLink;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDes;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellRestInfo;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMap;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMenu;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellRating;

// Other References
@property (weak, nonatomic) IBOutlet UIImageView *imgPic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblShortDes;
@property (weak, nonatomic) IBOutlet UILabel *lblMenu;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *lblCategories;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblOpeningHours;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblLink;
@property (weak, nonatomic) IBOutlet UITableView *mTableView;

@property (nonatomic, strong) DMResturant * currentResturant;

@end
