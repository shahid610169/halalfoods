//
//  ResturantsViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseMenuViewController.h"

typedef enum {
    ResturantModeNew,
    ResturantModePopular,
    ResturantModeClosest
} ResturantMode;


@interface ResturantsViewController : BaseMenuViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *modeSegment;

@property (weak, nonatomic) IBOutlet UITableView *mTableView;
- (IBAction)restModeChanged:(id)sender;
@end
