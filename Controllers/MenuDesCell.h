//
//  MenuDesCell.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 08/09/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuDesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
