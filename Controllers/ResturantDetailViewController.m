//
//  ResturantDetailViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "ResturantDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SDAnnotation.h"
#import "DMCategory.h"
#import "DMMenu.h"
#import <Social/Social.h>
#import "WebViewController.h"
#import "MenuTitleCell.h"
#import "MenuDesCell.h"
#import "MBProgressHUD.h"

@interface ResturantDetailViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>
{
    NSMutableArray * cellsArray;
    NSMutableArray * categories;
    NSMutableDictionary * menuDict;
}
@end

@implementation ResturantDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

-(void) addNavButtons
{
    UIBarButtonItem * btnShare = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"share", @"Share") style:UIBarButtonItemStyleBordered target:self action:@selector(btnShareClicked:)];
    self.navigationItem.rightBarButtonItem = btnShare;
}

-(void) btnShareClicked:(id)sender
{
    UIActionSheet * shareSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"share", @"Share") delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"fbShare", @"Share on Facebook") , NSLocalizedString(@"twtrShare", @"Share on Twitter"), nil];
    shareSheet.tag = 100;
    [shareSheet showInView:self.view];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = _currentResturant.name;
    cellsArray =[[NSMutableArray alloc] initWithObjects:_cellRestInfo,_cellImage , _cellDes, _cellMenu, _cellMap, _cellAddress, _cellOpeningHours, _cellPhoneNo, _cellLink, nil];
    categories = [[NSMutableArray alloc] init];
    menuDict = [[NSMutableDictionary alloc] init];
    
    [self loadData];
    [self getMenus];
    [self addNavButtons];
}

-(NSString *) getCategoriesString:(DMResturant*)rest
{
    NSString * category = @"";
    for (int i=0;i< rest.categories.count;i++) {
        DMCategory * cat = [rest.categories objectAtIndex:i];
        if (i != 0) {
            category = [category stringByAppendingString:@"/"];
        }
        category = [category stringByAppendingString:cat.name];
    }
    return category;
}

-(void) loadData
{
    _lblAddress.text = _currentResturant.address;
    _lblDescription.text = _currentResturant.shortDescription;
    _lblLink.text = _currentResturant.url;
    _lblOpeningHours.text = _currentResturant.openingHours;
    _lblPhoneNo.text = _currentResturant.phone;
    _lblName.text = _currentResturant.name;
    _lblShortDes.text = _currentResturant.shortDescription;

    // Description Size
    CGSize size = [_currentResturant.shortDescription sizeWithFont:[_lblDescription font] constrainedToSize:CGSizeMake(_lblDescription.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect frame = _lblDescription.frame;
    frame.size.height = size.height + 25;
    [_cellDes setFrame:frame];
    // Address Size
    size = [_currentResturant.address sizeWithFont:[_lblAddress font] constrainedToSize:CGSizeMake(_lblAddress.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    frame = _lblDescription.frame;
    frame.size.height = size.height + 25;
    [_cellAddress setFrame:frame];
    
    // Menu Size
//    NSString * menu = _currentResturant.menu;//[_currentResturant.menu stringByReplacingOccurrencesOfString:@"," withString:@"\n\r"];
//    size = [menu sizeWithFont:[_lblMenu font] constrainedToSize:CGSizeMake(_lblMenu.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
//    frame = _cellMenu.frame;
//    frame.size.height += size.height;
//    [_cellMenu setFrame:frame];
//    
//    
//    [_lblMenu setText:menu];
    
    // Create Map Pin and Navigate to it
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(_currentResturant.map.latitude, _currentResturant.map.longitude);
    SDAnnotation * annotation = [[SDAnnotation alloc] initWithTitle:_currentResturant.name AndCoordinate:location];
    [self.mapView addAnnotation:annotation];
    MKCoordinateRegion mapRegion;
    mapRegion.center = location;
    mapRegion.span.latitudeDelta = .005;
    mapRegion.span.longitudeDelta = .005;
    [_mapView setRegion:mapRegion animated: YES];
    
    [_imgPic setImageWithURL:[NSURL URLWithString:_currentResturant.picImage.url]];
    [_imgLogo setImageWithURL:[NSURL URLWithString:_currentResturant.logoImage.url]];
    [_lblCategories setText:[self getCategoriesString:_currentResturant]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [cellsArray objectAtIndex:indexPath.row];
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellsArray.count  /*+ categories.count + _currentResturant.menus.count*/;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [cellsArray objectAtIndex:indexPath.row];
    return cell.frame.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell * cell = [cellsArray objectAtIndex:indexPath.row];
    if ([cell isEqual:_cellLink]) {
        // Open link
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_currentResturant.url]];
    }
    else if ([cell isEqual:_cellPhoneNo]){
        UIActionSheet * phonePicker = [[UIActionSheet alloc] initWithTitle:_currentResturant.phone delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") destructiveButtonTitle:NSLocalizedString(@"callNumber", @"Call Number")  otherButtonTitles:nil, nil];
        [phonePicker showInView:self.view];
    }
}

#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100) {
        // Sharing Sheet
        if (buttonIndex == 0) {
            // Facebook
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                SLComposeViewController * facebookController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [facebookController setInitialText:_currentResturant.name];
                [facebookController addURL:[NSURL URLWithString:_currentResturant.url]];
                [self presentViewController:facebookController animated:YES completion:nil];
            }
            else{
                [self openWebController:WebViewTypeFacebook];
            }
        }
        else if(buttonIndex == 1){
            // Twitter
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                SLComposeViewController * twitterController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [twitterController setInitialText:_currentResturant.name];
                [twitterController addURL:[NSURL URLWithString:_currentResturant.url]];
                [self presentViewController:twitterController animated:YES completion:nil];
            }
            else{
                [self openWebController:WebViewTypeTwitter];
            }
        }
    }
    else{
        if (buttonIndex == 0) {
            // Canceled
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", _currentResturant.phone]]];
        }
    }
    
}

-(void)processMenus
{
    [categories removeAllObjects];
    [menuDict removeAllObjects];
    int index = 4;
    for (DMMenu * menu in _currentResturant.menus) {
        if ([menuDict valueForKey:menu.category.name]) {
            NSMutableArray * menuArray = [menuDict valueForKey:menu.category.name];
            [menuArray addObject:menu];
            MenuDesCell * desCell = [self createMenuCell:menu];
            [cellsArray insertObject:desCell atIndex:index++];
        }
        else{
            NSMutableArray * menuArray = [[NSMutableArray alloc] init];
            [menuArray addObject:menu];
            [menuDict setObject:menuArray forKey:menu.category.name];
            [categories addObject:menu.category.name];
            
            MenuTitleCell * titleCell = [self createCategoryCell:menu.category.name];
            [cellsArray insertObject:titleCell atIndex:index++];
            
            MenuDesCell * desCell = [self createMenuCell:menu];
            [cellsArray insertObject:desCell atIndex:index++];
            
        }
    }
    
}

-(MenuTitleCell*) createCategoryCell:(NSString*)category
{
    NSArray * cellArray = [[NSBundle mainBundle] loadNibNamed:@"MenuTitleCell" owner:nil options:nil];
    MenuTitleCell * titleCell = [cellArray objectAtIndex:0];
    [titleCell.lblTitle setText:category];
    
    return titleCell;
}

-(MenuDesCell*) createMenuCell:(DMMenu*)menu
{
    NSArray * cellArray = [[NSBundle mainBundle] loadNibNamed:@"MenuDesCell" owner:nil options:nil];
    MenuDesCell * desCell = [cellArray objectAtIndex:0];
    [desCell.lblTitle setText:menu.name];
    [desCell.lblDescription setText:menu.description];
    //Cell Size
    CGSize size = [menu.description sizeWithFont:[desCell.lblDescription font] constrainedToSize:CGSizeMake(desCell.lblDescription.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGRect rect = desCell.frame;
    rect.size.height += size.height - 20;
    [desCell setFrame:rect];
    
    return desCell;
}


-(void) getMenus
{
    NSDictionary * params = @{@"resturant": _currentResturant.objectId};
    [PFCloud callFunctionInBackground:@"getMenus" withParameters:params block:^(NSArray *result, NSError *error) {
        if (!error) {
            if (!_currentResturant.menus) {
                _currentResturant.menus = [[NSMutableArray alloc] init];
            }
            else{
                [_currentResturant.menus removeAllObjects];
            }
            for (PFObject *object in result) {
                DMMenu * menu = [[DMMenu alloc] initWithObject:object];
                [_currentResturant.menus addObject:menu];
            }
            [self processMenus];
            [self.mTableView reloadData];
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Internet Connection Error") withTitle:NSLocalizedString(@"appName", @"Application Error")];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}


-(void)openWebController:(WebViewType)type
{
    WebViewController * webController = [[WebViewController alloc] init];
    webController.type = type;
    [self.navigationController pushViewController:webController animated:YES];
}

#pragma mark - Dealoc
-(void)dealloc
{
    cellsArray = nil;
}

@end
