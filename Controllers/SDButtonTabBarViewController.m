//
//  SDButtonTabBarViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 09/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "SDButtonTabBarViewController.h"

@interface SDButtonTabBarViewController ()

@end

@implementation SDButtonTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSLog(@"TabBar");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"Tab Bar : %@", NSStringFromCGRect(self.view.frame));
    
    [self.view setFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height - 100)];
    
    
//    CGRect tabBarRect = self.tabBar.frame;
//    tabBarRect.origin.y -= 400;
//    [self.tabBar setFrame:tabBarRect];
//    
//    CGRect viewRect = self.view.frame;
//    viewRect.origin.y = tabBarRect.size.height;
//    [self.view setFrame:viewRect];
    // Do any additional setup after loading the view from its nib.
}

-(void)setViewControllers:(NSArray *)viewControllers
{
    [super setViewControllers:viewControllers];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
