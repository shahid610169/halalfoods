//
//  DealsDetailViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 01/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DMDeal.h"
#import <MapKit/MapKit.h>

@interface DealsDetailViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *mTableView;

// Cells
@property (strong, nonatomic) IBOutlet UITableViewCell *cellPhone;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellMap;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellAddress;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellLink;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellInfo;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellDescription;
// Others
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblLink;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblDeal;


@property (nonatomic, strong) DMDeal * currentDeal;

@end
