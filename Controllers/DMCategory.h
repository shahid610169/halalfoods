//
//  DMCategory.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMCategory : NSObject

@property (nonatomic, copy) NSString * name;
-(id)initWithObject:(PFObject*)obj;

@end
