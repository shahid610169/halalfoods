//
//  BaseViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([UIScreen mainScreen].bounds.size.height > 480) {
        // iPhone5
        CGRect frame = self.view.frame;
        frame.size.height = 568;
        [self.view setFrame:frame];
    }
    else{
        CGRect frame = self.view.frame;
        frame.size.height = 480;
        [self.view setFrame:frame];
    }
    
    if (IOS_VERSION > 6) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:112/255.0f green:0/255.0f blue:7/255.0f alpha:1.0f];
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    }else{
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:112/255.0f green:0/255.0f blue:7/255.0f alpha:1.0f]];
    }
    
    self.navigationController.navigationBar.opaque = YES;
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
