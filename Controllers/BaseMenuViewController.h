//
//  BaseMenuViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface BaseMenuViewController : UIViewController

-(void) resignAllResponders;

@end
