//
//  ResturantsViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "ResturantsViewController.h"
#import "DMResturant.h"
#import "ResturantDetailViewController.h"
#import "ResturantTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "SearchViewController.h"
#import "DMCategory.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"


@interface ResturantsViewController ()
{
    NSMutableArray * dataArray;
    ResturantMode restMode;
    PFGeoPoint * userLocation;
    AppDelegate * app;
    int previousSelectedIndex;
}

@end

@implementation ResturantsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"resturants", @"Resturants");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dataArray = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
    app = [UIApplication sharedApplication].delegate;
    if (app.currentLocation == nil) {
        [self getUserLocation];
    }
    else{
        userLocation = app.currentLocation;
        [self getResturants:ResturantModeNew];
    }
    [self addRefreshControl];
    
    [_modeSegment setTitle:NSLocalizedString(@"new", @"New") forSegmentAtIndex:0];
    [_modeSegment setTitle:NSLocalizedString(@"popular", @"Popular") forSegmentAtIndex:1];
    [_modeSegment setTitle:NSLocalizedString(@"closest", @"Closest") forSegmentAtIndex:2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Refresh Control
-(void) addRefreshControl
{
    UIRefreshControl * refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor colorWithRed:107/255.0f green:8/255.0f blue:127/255.0f alpha:1.0f];
    [refreshControl addTarget:self action:@selector(refreshClicked:) forControlEvents:UIControlEventValueChanged];
    [self.mTableView addSubview:refreshControl];
}

-(void) refreshClicked:(id)sender
{
    UIRefreshControl * refreshControl = (UIRefreshControl*)sender;
    [refreshControl endRefreshing];
    
    [self getResturants:(ResturantMode)_modeSegment.selectedSegmentIndex];
    
}

-(NSString *) getCategoriesString:(DMResturant*)rest
{
    NSString * category = @"";
    for (int i=0;i< rest.categories.count;i++) {
        DMCategory * cat = [rest.categories objectAtIndex:i];
        if (i != 0) {
            category = [category stringByAppendingString:@"/"];
        }
        category = [category stringByAppendingString:cat.name];
    }
    return category;
}

#pragma mark - IBAction

- (IBAction)restModeChanged:(id)sender {
    UISegmentedControl * segRestMode = (UISegmentedControl*)sender;
    if (segRestMode.selectedSegmentIndex == ResturantModePopular) {
        SearchViewController * searchController = [[SearchViewController alloc] init];
        searchController.searchType = SearchTypeSok;
        [self.navigationController pushViewController:searchController animated:YES];
        segRestMode.selectedSegmentIndex = previousSelectedIndex;
    }
    else{
        [self getResturants:(ResturantMode)segRestMode.selectedSegmentIndex];
        previousSelectedIndex = segRestMode.selectedSegmentIndex;
    }
    
}

-(void) getUserLocation
{
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if (!error) {
            userLocation = geoPoint;
            app.currentLocation = geoPoint;
            [self getResturants:(ResturantMode)_modeSegment.selectedSegmentIndex];
        }
        else{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [Utility showAlert:NSLocalizedString(@"LocationError", @"Location Error") withTitle:NSLocalizedString(@"appName", @"Application Name")];
        }
    }];
}

#pragma mark - API Methods

-(void)getResturants:(ResturantMode)mode
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (userLocation == nil) {
        [self getUserLocation];
        return;
    }

    
    NSDictionary * params = nil;
    switch (mode) {
        case ResturantModeClosest:
        {
            params = @{@"mode" : @"nearest", @"lat" : [NSNumber numberWithDouble:userLocation.latitude], @"lng" : [NSNumber numberWithDouble:userLocation.longitude]};
        }
            break;
        case ResturantModeNew:
        {
            params = @{@"mode" : @"new"};
        }
            break;
        case ResturantModePopular:
        {
            params = @{@"mode" : @"popular"};
        }
            break;
        default:
            break;
    }
    [PFCloud callFunctionInBackground:@"getResturants" withParameters:params block:^(NSArray *result, NSError *error) {
        if (!error) {
            [dataArray removeAllObjects];
            for (PFObject *object in result) {
                DMResturant * resturant = [[DMResturant alloc] initWithObject:object];
                [dataArray addObject:resturant];
            }
            [self.mTableView reloadData];
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Internet Connection Error") withTitle:NSLocalizedString(@"appName", @"Application Error")];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
    
}


#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ResturantTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"resturantCell"];
    if (!cell) {
        NSArray * nibsArray = [[NSBundle mainBundle] loadNibNamed:@"ResturantTableViewCell" owner:nil options:nil];
        cell = [nibsArray objectAtIndex:0];
    }
    DMResturant * resturant = [dataArray objectAtIndex:indexPath.row];
    [cell.imgLogo setImageWithURL:[NSURL URLWithString:resturant.logoImage.url] placeholderImage:[UIImage imageNamed:@"no_image"]];
    [cell.lblName setText:resturant.name];
    [cell.lblDescription setText:resturant.shortDescription];
    [cell.lblCategory setText:[self getCategoriesString:resturant]];
    
    float distance = [Utility distanceFromLocation:userLocation toLocation:resturant.map];
    
    [cell.lblDistance setText:[NSString stringWithFormat:@"%.2f KM", distance]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DMResturant * resturant= [dataArray objectAtIndex:indexPath.row];
    ResturantDetailViewController * detailController = [[ResturantDetailViewController alloc] init];
    detailController.currentResturant = resturant;
    [self.navigationController pushViewController:detailController animated:YES];
}


@end
