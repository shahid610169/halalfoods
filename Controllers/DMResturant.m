//
//  DMResturant.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "DMResturant.h"
#import "DMCategory.h"

@implementation DMResturant

-(id)initWithObject:(PFObject *)object
{
    self = [super init];
    if (self) {
        
        PFObject * obj = [object valueForKey:@"Resturant"];
        
        self.objectId = [obj valueForKey:@"objectId"];
        self.address = [obj valueForKey:@"address"];
        self.logoImage = [obj valueForKey:@"logoImage"];
        self.map = [obj valueForKey:@"map"];
        self.menu = [obj valueForKey:@"menu"];
        self.name = [obj valueForKey:@"name"];
        self.openingHours = [obj valueForKey:@"openingHours"];
        self.phone = [obj valueForKey:@"phone"];
        self.picImage = [obj valueForKey:@"picImage"];
        self.shortDescription = [obj valueForKey:@"shortDescription"];
        self.url = [obj valueForKey:@"url"];
        
        _categories = [[NSMutableArray alloc] init];
        NSArray * cats = [object valueForKey:@"Categories"];
        for (PFObject * dummyCat in cats) {
            
            PFObject * category = [dummyCat objectForKey:@"Category"];
            
            DMCategory * cat = [[DMCategory alloc] initWithObject:category];
            [_categories addObject:cat];
        }
        
        
    }
    return self;
}

-(id)initWithPFObject:(PFObject *)obj
{
    self = [super init];
    if (self) {
        
        self.objectId = [obj valueForKey:@"objectId"];
        self.address = [obj valueForKey:@"address"];
        self.logoImage = [obj valueForKey:@"logoImage"];
        self.map = [obj valueForKey:@"map"];
        self.menu = [obj valueForKey:@"menu"];
        self.name = [obj valueForKey:@"name"];
        self.openingHours = [obj valueForKey:@"openingHours"];
        self.phone = [obj valueForKey:@"phone"];
        self.picImage = [obj valueForKey:@"picImage"];
        self.shortDescription = [obj valueForKey:@"shortDescription"];
        self.url = [obj valueForKey:@"url"];
        
    }
    return self;
}

@end
