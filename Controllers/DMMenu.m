//
//  DMMenu.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 08/09/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "DMMenu.h"

@implementation DMMenu

-(id)initWithObject:(PFObject *)obj
{
    self = [super init];
    if (self) {
        self.objectId = [obj valueForKey:@"objectId"];
        self.name = [obj valueForKey:@"Name"];
        self.description = [obj valueForKey:@"Description"];
        
        PFObject * category = [obj valueForKey:@"Category"];
        self.category = [[DMCategory alloc] initWithObject:category];
        
    }
    return self;
}

@end
