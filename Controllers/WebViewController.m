//
//  WebViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 05/09/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (_type == WebViewTypeFacebook) {
        self.title = @"Facebook";
    }
    else if(_type == WebViewTypeTwitter){
        self.title = @"Twitter";
    }
    [self addNavButton];
    [self loadRequest];
}

-(void) addNavButton
{
    UIBarButtonItem * btnRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshPage:)];
    self.navigationItem.rightBarButtonItem = btnRefresh;
}

-(void) refreshPage:(id)sender
{
    [self loadRequest];
}

-(void)loadRequest
{
    NSString * url = nil;
    if (_type == WebViewTypeFacebook) {
        url = @"http://facebook.com";
    }
    else if (_type == WebViewTypeTwitter){
        url = @"http://twitter.com";
    }
    
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
