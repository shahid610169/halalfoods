//
//  BaseMenuViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "BaseMenuViewController.h"
#import "PKRevealController.h"

@interface BaseMenuViewController ()
{
    UIBarButtonItem * btnMenu;
}
@end

@implementation BaseMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.navigationController.viewControllers.count<=1) {
        [self addMenu];
    }
    
    if ([UIScreen mainScreen].bounds.size.height > 480) {
        // iPhone5
        CGRect frame = self.view.frame;
        frame.size.height = 568;
        [self.view setFrame:frame];
    }
    else{
        CGRect frame = self.view.frame;
        frame.size.height = 480;
        [self.view setFrame:frame];
    }
    
    self.navigationController.navigationBar.opaque = YES;
    self.navigationController.navigationBar.translucent = NO;
    
    
    // Do any additional setup after loading the view.
}

-(void)addMenu
{
    // Menu Button
    UIButton * menu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 25)];
    [menu setBackgroundImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [menu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    btnMenu = [[UIBarButtonItem alloc] initWithCustomView:menu];
    self.navigationItem.leftBarButtonItem =  btnMenu;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (IOS_VERSION > 6) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:225/255.0f green:221/255.0f blue:111/255.0f alpha:1.0f];
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:112/255.0f green:0/255.0f blue:7/255.0f alpha:1.0f]];
    }else{
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:225/255.0f green:221/255.0f blue:111/255.0f alpha:1.0f]];
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:112/255.0f green:0/255.0f blue:7/255.0f alpha:1.0f], NSForegroundColorAttributeName, nil]];
}

-(void) btnMenuClicked:(id)sender
{
    if (self.revealController.state == PKRevealControllerShowsFrontViewController) {
        // Open Menu Controller
        [self.revealController showViewController:self.revealController.leftViewController animated:YES completion:nil];
    }
    else if (self.revealController.state == PKRevealControllerShowsLeftViewController){
        [self.revealController showViewController:self.revealController.frontViewController animated:YES completion:nil];
    }
    [self resignAllResponders];
}

-(void) resignAllResponders
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    btnMenu = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
