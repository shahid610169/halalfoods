//
//  MenuViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "MenuViewController.h"
#import "MainViewController.h"
#import "ResturantsViewController.h"
#import "PKRevealController.h"
#import "SDSegmentTabBarController.h"
#import "DealsViewController.h"
#import "SearchViewController.h"
#import "MHTabBarController.h"
#import "SettingsViewController.h"

@interface MenuViewController ()
{
    NSMutableArray * dataArray;
}
@end

@implementation MenuViewController

-(id)init
{
    self = [super init];
    if (self) {
        [self initalizeMenuItems];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initalizeMenuItems];
    }
    return self;
}

-(void) initalizeMenuItems
{
    dataArray = [[NSMutableArray alloc] init];
    //[dataArray addObject:@{@"title": @"Home", @"icon" : @"iconHome", @"tag" : @"0"}];
    [dataArray addObject:@{@"title": NSLocalizedString(@"resturants", @"Resturants"), @"icon" : @"iconResturant", @"tag" : @"1"}];
    [dataArray addObject:@{@"title": NSLocalizedString(@"deals", @"Deals"), @"icon" : @"iconDeal", @"tag" : @"2"}];
    //[dataArray addObject:@{@"title": @"Settings", @"icon" : @"iconSettings", @"tag" : @"3"}];
    [dataArray addObject:@{@"title": NSLocalizedString(@"search", @"Search"), @"icon" : @"iconSearch", @"tag" : @"4"}];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if ([[UIDevice currentDevice].systemVersion integerValue]>6) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewControllerDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSDictionary * dict = [dataArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict valueForKey:@"title"];
    cell.imageView.image = [UIImage imageNamed:[dict valueForKey:@"icon"]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController * topController = self.revealController.frontViewController;
    NSDictionary * dict = [dataArray objectAtIndex:indexPath.row];
    ControllerType type = [[dict valueForKey:@"tag"] integerValue];
    if (type == ControllerTypeHome)
    {
        if (!([topController isKindOfClass:[UINavigationController class]] && [((UINavigationController*)topController).topViewController isKindOfClass:[MHTabBarController class]]))
        {
            
            ResturantsViewController * resturantController = [[ResturantsViewController alloc] init];
            DealsViewController * dealsController = [[DealsViewController alloc] init];
            
            // Tab Bar Controller
            MHTabBarController * tabController = [[MHTabBarController alloc] init];
            [tabController setViewControllers:@[resturantController, dealsController]];
            
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:tabController];
            
            
            [self.revealController setFrontViewController:navController];
        }
        
    }
    else if (type == ControllerTypeResturant)
    {
        if (!([topController isKindOfClass:[UINavigationController class]] && [((UINavigationController*)topController).topViewController isKindOfClass:[MHTabBarController class]]))
        {
            
            ResturantsViewController * resturantController = [[ResturantsViewController alloc] init];
            DealsViewController * dealsController = [[DealsViewController alloc] init];
            
            // Tab Bar Controller
            MHTabBarController * tabController = [[MHTabBarController alloc] init];
            [tabController setViewControllers:@[resturantController, dealsController]];
            
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:tabController];
             [tabController setSelectedIndex:0];
            
            [self.revealController setFrontViewController:navController];
        }
        else if([topController isKindOfClass:[UINavigationController class]]){
            MHTabBarController * tabController = (MHTabBarController*)((UINavigationController*)topController).topViewController;
            if (tabController.selectedIndex == 1) {
                [tabController setSelectedIndex:0];
            }
        }
    }
    else if (type == ControllerTypeDeals)
    {
        if (!([topController isKindOfClass:[UINavigationController class]] && [((UINavigationController*)topController).topViewController isKindOfClass:[MHTabBarController class]]))
        {
            ResturantsViewController * resturantController = [[ResturantsViewController alloc] init];
            DealsViewController * dealsController = [[DealsViewController alloc] init];
            
            // Tab Bar Controller
            MHTabBarController * tabController = [[MHTabBarController alloc] init];
            [tabController setViewControllers:@[resturantController, dealsController]];
            
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:tabController];
            [tabController setSelectedIndex:1];
            
            [self.revealController setFrontViewController:navController];
        }
        else if([topController isKindOfClass:[UINavigationController class]]){
            MHTabBarController * tabController = (MHTabBarController*)((UINavigationController*)topController).topViewController;
            if (tabController.selectedIndex == 0) {
                [tabController setSelectedIndex:1];
            }
        }
    }
    else if (type == ControllerTypeSettings) {
        if (([topController isKindOfClass:[UINavigationController class]] && ![((UINavigationController*)topController).topViewController isKindOfClass:[SettingsViewController class]]) || ([topController isKindOfClass:[SDSegmentTabBarController class]]))
        {
            SettingsViewController * settingsController = [[SettingsViewController alloc] init];
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:settingsController];
            [self.revealController setFrontViewController:navController];
        }
    }
    else if (type == ControllerTypeSearch) {
        if (([topController isKindOfClass:[UINavigationController class]] && ![((UINavigationController*)topController).topViewController isKindOfClass:[SearchViewController class]]) || ([topController isKindOfClass:[SDSegmentTabBarController class]]))
        {
            SearchViewController * searchController = [[SearchViewController alloc] init];
            UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:searchController];
            [self.revealController setFrontViewController:navController];
        }
    }
    [self.revealController showViewController:self.revealController.frontViewController animated:YES completion:nil];
}

@end
