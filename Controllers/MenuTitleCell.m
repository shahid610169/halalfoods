//
//  MenuTitleCell.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 08/09/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "MenuTitleCell.h"

@implementation MenuTitleCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
