//
//  WebViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 05/09/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef enum{
    WebViewTypeFacebook,
    WebViewTypeTwitter
}WebViewType;

@interface WebViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) WebViewType type;

@end
