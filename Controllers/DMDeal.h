//
//  DMDeal.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 01/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMDeal : NSObject

@property (nonatomic, copy) NSString * objectId;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, copy) NSString * deal;
@property (nonatomic, copy) NSString * endDate;
@property (nonatomic, copy) NSString * startDate;
@property (nonatomic, copy) NSString * openingHours;
@property (nonatomic, copy) NSString * phone;
@property (nonatomic, copy) NSString * shortDes;
@property (nonatomic, copy) NSString * url;

@property (nonatomic, copy) PFGeoPoint * geoLocation;
@property (nonatomic, copy) PFFile * logoImage;
@property (nonatomic, retain) NSMutableArray * categories;

-(id)initWithObject:(PFObject*)obj;


@end
