//
//  SDLocation.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMLocation : NSObject

@property (nonatomic, copy) NSString * longitude;
@property (nonatomic, copy) NSString * latitude;

-(id)initWithDict:(NSDictionary*)dict;

@end
