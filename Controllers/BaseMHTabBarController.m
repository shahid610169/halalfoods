//
//  BaseMHTabBarController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 14/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "BaseMHTabBarController.h"
#import "PKRevealController.h"
#import "SearchViewController.h"

@interface BaseMHTabBarController ()
{
    UIBarButtonItem * btnMenu;
}
@end

@implementation BaseMHTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Title Logo
//    UIImageView * titleView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
//    [titleView setImage:[UIImage imageNamed:@"logo"]];
//    [self.navigationItem setTitleView:titleView];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", @"back") style:UIBarButtonItemStylePlain target:nil action:nil];

    
    self.title = NSLocalizedString(@"appName", @"Application");
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:112/255.0f green:0/255.0f blue:7/255.0f alpha:1.0f], NSForegroundColorAttributeName, nil]];
    
    // Navigation buttons
    [self addNavBarButtons];
}


-(void) addNavBarButtons
{
    // Left Menu
    UIButton * menu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 25)];
    [menu setBackgroundImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [menu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    btnMenu = [[UIBarButtonItem alloc] initWithCustomView:menu];
    self.navigationItem.leftBarButtonItem =  btnMenu;
    
    // Right Search button
    UIBarButtonItem * searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonClicked:)];
    self.navigationItem.rightBarButtonItem = searchButton;
}

-(void) searchButtonClicked:(id)sender
{
    SearchViewController * searchController = [[SearchViewController alloc] init];
    [self.navigationController pushViewController:searchController animated:YES];
}

-(void) btnMenuClicked:(id)sender
{
    if (self.revealController.state == PKRevealControllerShowsFrontViewController) {
        // Open Menu Controller
        [self.revealController showViewController:self.revealController.leftViewController animated:YES completion:nil];
    }
    else if (self.revealController.state == PKRevealControllerShowsLeftViewController){
        [self.revealController showViewController:self.revealController.frontViewController animated:YES completion:nil];
    }
//    [self resignAllResponders];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
