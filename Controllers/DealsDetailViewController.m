//
//  DealsDetailViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 01/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "DealsDetailViewController.h"
#import "SDAnnotation.h"
#import "DMCategory.h"
#import "UIImageView+AFNetworking.h"
#import <Social/Social.h>
#import "WebViewController.h"

@interface DealsDetailViewController () <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>
{
    NSMutableArray * cellsArray;
}
@end

@implementation DealsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = _currentDeal.deal;
    cellsArray =[[NSMutableArray alloc] initWithObjects:_cellInfo, _cellMap, _cellDescription, _cellAddress, _cellPhone, _cellLink ,nil];
    [self loadData];
    [self addNavButtons];
}




-(void) addNavButtons
{
    UIBarButtonItem * btnShare = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"share", @"Share") style:UIBarButtonItemStyleBordered target:self action:@selector(btnShareClicked:)];
    self.navigationItem.rightBarButtonItem = btnShare;
}

-(void) btnShareClicked:(id)sender
{
    UIActionSheet * shareSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"share", @"Share")  delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"fbShare", @"Share on Facebook"), NSLocalizedString(@"twtrShare", @"Share On Twitter"), nil];
    shareSheet.tag = 100;
    [shareSheet showInView:self.view];
}


-(NSString *) getCategoriesString:(DMDeal*)rest
{
    NSString * category = @"";
    for (int i=0;i< rest.categories.count;i++) {
        DMCategory * cat = [rest.categories objectAtIndex:i];
        if (i != 0) {
            category = [category stringByAppendingString:@"/"];
        }
        category = [category stringByAppendingString:cat.name];
    }
    return category;
}

-(void) loadData
{
    _lblAddress.text = _currentDeal.address;
    _lblDescription.text = _currentDeal.shortDes;
    _lblLink.text = _currentDeal.url;
    _lblPhone.text = _currentDeal.phone;
    _lblDeal.text = _currentDeal.deal;
    
    // Description Size
    CGSize size = [_currentDeal.description sizeWithFont:[_lblDescription font] constrainedToSize:CGSizeMake(_lblDescription.frame.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    CGRect frame = _lblDescription.frame;
    frame.size.height = size.height + 25;
    [_cellDescription setFrame:frame];
    
    // Create Map Pin and Navigate to it
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(_currentDeal.geoLocation.latitude, _currentDeal.geoLocation.longitude);
    SDAnnotation * annotation = [[SDAnnotation alloc] initWithTitle:_currentDeal.deal AndCoordinate:location];
    [self.mapView addAnnotation:annotation];
    MKCoordinateRegion mapRegion;
    mapRegion.center = location;
    mapRegion.span.latitudeDelta = .005;
    mapRegion.span.longitudeDelta = .005;
    [_mapView setRegion:mapRegion animated: YES];
    
    [_imgLogo setImageWithURL:[NSURL URLWithString:_currentDeal.logoImage.url]];
    [_lblCategory setText:[self getCategoriesString:_currentDeal]];
}

-(void)openWebController:(WebViewType)type
{
    WebViewController * webController = [[WebViewController alloc] init];
    webController.type = type;
    [self.navigationController pushViewController:webController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [cellsArray objectAtIndex:indexPath.row];
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [cellsArray objectAtIndex:indexPath.row];
    return cell.frame.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell * cell = [cellsArray objectAtIndex:indexPath.row];
    if ([cell isEqual:_cellLink]) {
        // Open link
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_currentDeal.url]];
    }
    else if ([cell isEqual:_cellPhone]){
        UIActionSheet * phonePicker = [[UIActionSheet alloc] initWithTitle:_currentDeal.phone delegate:self cancelButtonTitle:NSLocalizedString(@"cancel",  @"Cancel") destructiveButtonTitle:NSLocalizedString(@"callNumber", @"Call This Number") otherButtonTitles:nil, nil];
        [phonePicker showInView:self.view];
    }
}

#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100) {
        // Sharing Sheet
        if (buttonIndex == 0) {
            // Facebook
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                SLComposeViewController * facebookController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [facebookController setInitialText:_currentDeal.deal];
                [facebookController addURL:[NSURL URLWithString:_currentDeal.url]];
                [self presentViewController:facebookController animated:YES completion:nil];
            }
            else{
                [self openWebController:WebViewTypeFacebook];
            }
        }
        else if(buttonIndex == 1){
            // Twitter
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                SLComposeViewController * twitterController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [twitterController setInitialText:_currentDeal.deal];
                [twitterController addURL:[NSURL URLWithString:_currentDeal.url]];
                [self presentViewController:twitterController animated:YES completion:nil];
            }
            else{
                [self openWebController:WebViewTypeTwitter];
            }
        }
    }
    else{
        if (buttonIndex == 0) {
            // Canceled
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", _currentDeal.phone]]];
        }
    }
    
}



@end
