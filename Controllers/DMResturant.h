//
//  DMResturant.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMLocation.h"
#import <Parse/Parse.h>

@interface DMResturant : NSObject

@property (nonatomic, copy) NSString * objectId;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, copy) NSString * openingHours;
@property (nonatomic, copy) NSString * phone;
@property (nonatomic, copy) NSString * url;
@property (nonatomic, copy) NSString * shortDescription;
@property (nonatomic, copy) NSString * description;
@property (nonatomic, copy) NSString * menu;
@property (nonatomic, copy) NSString * name;
@property (nonatomic, strong) PFGeoPoint * map;
@property (nonatomic, strong) PFFile * logoImage;
@property (nonatomic, strong) PFFile * picImage;
@property (nonatomic, strong) NSMutableArray * categories;
@property (nonatomic, strong) NSMutableArray * menus;

-(id)initWithObject:(PFObject*)obj;
-(id)initWithPFObject:(PFObject *)obj;


@end
