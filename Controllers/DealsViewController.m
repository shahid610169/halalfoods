//
//  DealsViewController.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "DealsViewController.h"
#import "ResturantTableViewCell.h"
#import "DMDeal.h"
#import "UIImageView+AFNetworking.h"
#import "DealsDetailViewController.h"
#import "SearchViewController.h"
#import "DMCategory.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"


@interface DealsViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray * dataArray;
    DealsMode dealsMode;
    PFGeoPoint * userLocation;
    AppDelegate * app;
    int previousSelectedIndex;
}

@end

@implementation DealsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"deals", @"Deals");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    dataArray = [[NSMutableArray alloc] init];
    app = [UIApplication sharedApplication].delegate;
    
    if (app.currentLocation == nil) {
        [self getUserLocation];
    }
    else{
        userLocation = app.currentLocation;
        [self getDeals:DealsModeNewest];
    }
    [self addRefreshControl];
    
    [_modeSegment setTitle:NSLocalizedString(@"new", @"New") forSegmentAtIndex:0];
    [_modeSegment setTitle:NSLocalizedString(@"popular", @"Popular") forSegmentAtIndex:1];
    [_modeSegment setTitle:NSLocalizedString(@"closest", @"Closest") forSegmentAtIndex:2];
    
}

-(void) addRefreshControl
{
    UIRefreshControl * refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor colorWithRed:107/255.0f green:8/255.0f blue:127/255.0f alpha:1.0f];
    [refreshControl addTarget:self action:@selector(refreshClicked:) forControlEvents:UIControlEventValueChanged];
    [self.mTableView addSubview:refreshControl];
}

-(void) refreshClicked:(id)sender
{
    UIRefreshControl * refreshControl = (UIRefreshControl*)sender;
    [refreshControl endRefreshing];
    [self getDeals:_modeSegment.selectedSegmentIndex];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *) getCategoriesString:(DMDeal*)rest
{
    NSString * category = @"";
    for (int i=0;i< rest.categories.count;i++) {
        DMCategory * cat = [rest.categories objectAtIndex:i];
        if (i != 0) {
            category = [category stringByAppendingString:@"/"];
        }
        category = [category stringByAppendingString:cat.name];
    }
    return category;
}

-(void) getUserLocation
{
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if (!error) {
            userLocation = geoPoint;
            app.currentLocation = geoPoint;
            [self getDeals:_modeSegment.selectedSegmentIndex];
        }
        else{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [Utility showAlert:NSLocalizedString(@"LocationError", @"Twitter Error")  withTitle:NSLocalizedString(@"appName", @"App Name")];
        }
    }];
}

#pragma mark - IBAction

- (IBAction)dealModeChanged:(id)sender {
    UISegmentedControl * dealsSegment = (UISegmentedControl*)sender;
    if (dealsSegment.selectedSegmentIndex == DealsModePopular) {
        SearchViewController * searchController = [[SearchViewController alloc] init];
        searchController.searchType = SearchTypeSok;
        [self.navigationController pushViewController:searchController animated:YES];
        dealsSegment.selectedSegmentIndex = previousSelectedIndex;
    }
    else{
        [self getDeals:dealsSegment.selectedSegmentIndex];
        previousSelectedIndex = dealsSegment.selectedSegmentIndex;
    }
}

#pragma mark - API Methods

-(void)getDeals:(DealsMode)mode
{
    
    if (userLocation == nil) {
        [self getUserLocation];
        return;
    }
    
    NSDictionary * params = nil;
    switch (mode) {
        case DealsModePopular:
        {
            params = @{@"mode" : @"popular"};
        }
            break;
        case DealsModeNewest:
        {
            params = @{@"mode" : @"new"};
        }
            break;
        case DealsModeClosest:
        {
            params = @{@"mode" : @"nearest", @"lat" : [NSNumber numberWithDouble:userLocation.latitude], @"lng" : [NSNumber numberWithDouble:userLocation.longitude]};
        }
            break;
        default:
            break;
    }
    
    [PFCloud callFunctionInBackground:@"getDeals" withParameters:params block:^(NSArray *result, NSError *error) {
        if (!error) {
            [dataArray removeAllObjects];
            for (PFObject *object in result) {
                DMDeal * deal = [[DMDeal alloc] initWithObject:object];
                [dataArray addObject:deal];
            }
            [self.mTableView reloadData];
        }
        else{
            [Utility showAlert:NSLocalizedString(@"InternetError", @"Twitter Error")  withTitle:NSLocalizedString(@"appName", @"App Name")];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

#pragma mark - UITableViewDelegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ResturantTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"resturantCell"];
    if (!cell) {
        NSArray * nibsArray = [[NSBundle mainBundle] loadNibNamed:@"ResturantTableViewCell" owner:nil options:nil];
        cell = [nibsArray objectAtIndex:0];
    }
    DMDeal * deal = [dataArray objectAtIndex:indexPath.row];
    [cell.imgLogo setImageWithURL:[NSURL URLWithString:deal.logoImage.url] placeholderImage:[UIImage imageNamed:@"no_image"]];
    [cell.lblName setText:deal.deal];
    [cell.lblDescription setText:deal.shortDes];
    [cell.lblCategory setText:[self getCategoriesString:deal]];
    float distance = [Utility distanceFromLocation:userLocation toLocation:deal.geoLocation];
    
    [cell.lblDistance setText:[NSString stringWithFormat:@"%.2f KM", distance]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DMDeal * deal= [dataArray objectAtIndex:indexPath.row];
    DealsDetailViewController * dealDetailController = [[DealsDetailViewController alloc] init];
    dealDetailController.currentDeal = deal;
    [self.navigationController pushViewController:dealDetailController animated:YES];
}


@end
