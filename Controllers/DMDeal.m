//
//  DMDeal.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 01/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "DMDeal.h"
#import "DMCategory.h"

@implementation DMDeal

-(id)initWithObject:(PFObject *)object
{
    self = [super init];
    if (self) {
        
        PFObject * obj = [object valueForKey:@"Deal"];
        
        self.objectId = [obj valueForKey:@"objectId"];
        self.address = [obj valueForKey:@"address"];
        self.deal = [obj valueForKey:@"deals"];
        self.endDate = [obj valueForKey:@"endDate"];
        self.shortDes = [obj valueForKey:@"shortDescription"];
        self.startDate = [obj valueForKey:@"startDate"];
        self.url = [obj valueForKey:@"url"];
        self.phone = [obj valueForKey:@"phone"];
        self.openingHours = [obj valueForKey:@"openingHours"];
        self.logoImage = [obj valueForKey:@"logoImage"];
        self.geoLocation = [obj valueForKey:@"geoLocation"];
        
        _categories = [[NSMutableArray alloc] init];
        NSArray * cats = [object valueForKey:@"Categories"];
        for (PFObject * dummyCat in cats) {
            
            PFObject * category = [dummyCat objectForKey:@"Category"];
            
            DMCategory * cat = [[DMCategory alloc] initWithObject:category];
            [_categories addObject:cat];
        }
        
    }
    return self;
}

@end
