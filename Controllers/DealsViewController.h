//
//  DealsViewController.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseMenuViewController.h"

typedef enum{
    DealsModeNewest,
    DealsModePopular,
    DealsModeClosest
}DealsMode;


@interface DealsViewController : BaseMenuViewController
@property (weak, nonatomic) IBOutlet UITableView *mTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *modeSegment;

- (IBAction)dealModeChanged:(id)sender;
@end
