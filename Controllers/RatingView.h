//
//  RatingView.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 08/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingView : UIView

@property (nonatomic) NSInteger rating;
@property (nonatomic) CGSize starSize;
@property (nonatomic) BOOL autoAdjust;
// Default number of starts are 5.
@property (nonatomic) NSInteger numberOfStars;

@end
