//
//  DMCategory.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 29/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "DMCategory.h"

@implementation DMCategory

-(id)initWithObject:(PFObject *)obj
{
    self = [super init];
    if (self) {
        self.name = [obj valueForKey:@"name"];
    }
    return self;
}

@end
