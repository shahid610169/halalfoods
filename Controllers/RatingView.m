//
//  RatingView.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 08/08/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "RatingView.h"
#define BTN_RATING_TAG 19999

@implementation RatingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _numberOfStars = 5;
        //[self createStarts];
    }
    return self;
}

-(void) createStarts
{
    for (int i=0; i<_numberOfStars; i++) {
        int starWidth = self.frame.size.width/ _numberOfStars;
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(starWidth * i, 0, starWidth, self.frame.size.height)];
        [btn setBackgroundImage:[UIImage imageNamed:@"star_empty"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"star_full"] forState:UIControlStateSelected];
        [btn setTag:(BTN_RATING_TAG + i)];
        if (_rating >= (i+1)) {
            [btn setSelected:YES];
        }
        else{
            [btn setSelected:NO];
        }
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
}

-(void) setRating:(NSInteger)rating
{
    _rating = rating;
    for (int i=0; i<_numberOfStars; i++) {
        UIButton * btn = (UIButton*)[self viewWithTag:(BTN_RATING_TAG + i)];
        if (_rating >= (i+1)) {
            [btn setSelected:YES];
        }
        else{
            [btn setSelected:NO];
        }
    }
}

-(void) btnClicked:(id)sender{
    NSInteger tag = ((UIButton*)sender).tag;
    [self setRating:(tag - BTN_RATING_TAG)+1];
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self createStarts];
}

@end
