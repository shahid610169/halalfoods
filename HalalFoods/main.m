//
//  main.m
//  HalalFoods
//
//  Created by confiz on 14/09/2014.
//  Copyright (c) 2014 confiz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"sv", nil]
                                              forKey:@"AppleLanguages"];
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
