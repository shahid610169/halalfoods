//
//  Utility.m
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import "Utility.h"
#import <CoreLocation/CoreLocation.h>

@implementation Utility

+(void) showAlert:(NSString*)msg withTitle:(NSString*)title
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

+(float) distanceFromLocation:(PFGeoPoint*)fromLocation toLocation:(PFGeoPoint*)toLocation
{
    if (fromLocation == nil || toLocation == nil) {
        return 0;
    }
    CLLocation *currentLoc = [[CLLocation alloc] initWithLatitude:fromLocation.latitude longitude:fromLocation.longitude];
    CLLocation *restaurnatLoc = [[CLLocation alloc] initWithLatitude:toLocation.latitude longitude:toLocation.longitude];
    CLLocationDistance meters = [restaurnatLoc distanceFromLocation:currentLoc];
    return meters/1000;
}

@end
