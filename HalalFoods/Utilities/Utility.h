//
//  Utility.h
//  HilalFoods
//
//  Created by Shahid Nasrullah on 30/07/2014.
//  Copyright (c) 2014 Shahid. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    RequestModeNew,
    RequestModePopular,
    RequestModeClosest
}RequestMode;

@interface Utility : NSObject

+(void) showAlert:(NSString*)msg withTitle:(NSString*)title;
+(float) distanceFromLocation:(PFGeoPoint*)fromLocation toLocation:(PFGeoPoint*)toLocation;

@end
