//
//  AppDelegate.h
//  HalalFoods
//
//  Created by confiz on 14/09/2014.
//  Copyright (c) 2014 confiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) PFGeoPoint * currentLocation;

-(void) updateUserLocation;

@end
