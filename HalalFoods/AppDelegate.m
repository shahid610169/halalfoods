//
//  AppDelegate.m
//  HalalFoods
//
//  Created by confiz on 14/09/2014.
//  Copyright (c) 2014 confiz. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "MenuViewController.h"
#import "MainViewController.h"
#import "PKRevealController.h"
#import "SDSegmentTabBarController.h"
#import "SDButtonTabBarViewController.h"
#import "MHTabBarController.h"
#import "ResturantsViewController.h"
#import "DealsViewController.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [Parse setApplicationId:@"kJwhmuyZ0foFNcdfAek9iPamcZnYwFH7oq3MBr8v"
                  clientKey:@"jwAsQVfwa1OW4oH9YTm8Zy3zwi38fGsxlmEviACJ"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    // Root Window Initialization
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    MenuViewController * menuController = [[MenuViewController alloc] init];
    
    ResturantsViewController * resturantController = [[ResturantsViewController alloc] init];
    DealsViewController * dealsController = [[DealsViewController alloc] init];
    
    // Tab Bar Controller
    MHTabBarController * tabController = [[MHTabBarController alloc] init];
    [tabController setViewControllers:@[resturantController, dealsController]];
    
    UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:tabController];
    
    PKRevealController * revealController = [PKRevealController revealControllerWithFrontViewController:navController leftViewController:menuController];
    revealController.delegate = self;
    
    [self updateUserLocation];
    self.window.rootViewController = revealController;
    
    
    [self.window makeKeyAndVisible];
    return YES;
}

-(void) updateUserLocation
{
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if (!error) {
            self.currentLocation = geoPoint;
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserLocationUpdatedNotification object:nil userInfo:nil];
        }
    }];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
